# rikhw

A server-side clojure app for getting footy scores from the [football-data api](https://football-data.org)

![clojure logo](resources/images/clojure-logo.png "Clojure rulez")

[![Build Status](https://travis-ci.com/emgullufsen/clojure-webapp-footy.svg?branch=master)](https://travis-ci.com/gitlab/emgullufsen/clojure-webapp-footy)
